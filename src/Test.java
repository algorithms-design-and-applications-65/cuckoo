public class test {

    public static void main(String[] args) {
        Cuckoo cuckoo = new Cuckoo();
        cuckoo.put(100, "Somake");
        cuckoo.put(3, "Sakeom");
        cuckoo.put(101, "Sakda");
        
        System.out.println(cuckoo.get(100).toString());
        System.out.println(cuckoo.get(3).toString());
        System.out.println(cuckoo.get(101).toString());
        cuckoo.remove(100);
        System.out.println(cuckoo.get(100));

    }
}

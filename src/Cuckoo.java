
public class Cuckoo {

    private int key;
    private String value;
    String Valuet;
    int Keyt;
    private Cuckoo[] T0 = new Cuckoo[100];
    private Cuckoo[] T1 = new Cuckoo[100];

    public Cuckoo(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public Cuckoo() {

    }

    private int getKey(int key) {
        return key;
    }

    private String getValue(String value) {
        return value;
    }

    public int hash0(int key) {
        return key % T0.length;
    }

    public int hash1(int key) {
        return (key / 100) % T1.length;
    }

    public void put(int key, String value) {

        if (T0[hash0(key)] != null && T0[hash0(key)].key == key) {
            T0[hash0(key)].key = key;
            T0[hash0(key)].value = value;
            return;
        }
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            T1[hash1(key)].key = key;
            T1[hash1(key)].value = value;
            return;
        }
        int i = 0;

        while (true) {
            if (i == 0) {
                if (T0[hash0(key)] == null) {
                    T0[hash0(key)] = new Cuckoo();
                    T0[hash0(key)].key = key;
                    T0[hash0(key)].value = value;
                    return;
                }
            } else {
                if (T1[hash1(key)] == null) {
                    T1[hash1(key)] = new Cuckoo();
                    T1[hash1(key)].key = key;
                    T1[hash1(key)].value = value;
                    return;
                }
            }

            if (i == 0) {
                Valuet = T0[hash0(key)].value;
                Keyt = T0[hash0(key)].key;
                T0[hash0(key)].key = key;
                T0[hash0(key)].value = value;
                key = Keyt;
                value = Valuet;

            } else {
                Valuet = T1[hash1(key)].value;
                Keyt = T1[hash1(key)].key;
                T1[hash1(key)].key = key;
                T1[hash1(key)].value = value;
                key = Keyt;
                value = Valuet;
            }
            i = (i + 1) % 2;
        }
    }

    public Cuckoo get(int key) {
        if (T0[hash0(key)] != null && T0[hash0(key)].key == key) {
            return T0[hash0(key)];
        }
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            return T1[hash1(key)];
        }
        return null;
    }

    public Cuckoo remove(int key) {
        if (T0[hash0(key)] != null && T0[hash0(key)].key == key) {
            Cuckoo temp = T0[hash0(key)];
            T0[hash0(key)] = null;
            return temp;
        }
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            Cuckoo temp = T1[hash1(key)];
            T1[hash1(key)] = null;
            return temp;
        }
        return null;

    }

    public String toString() {
        return "Key: " + key + " Value: " + value;
    }

}
